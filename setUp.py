import csv
from collections import Counter
import numpy as np
import pandas as pd

import gc

# nltk packages used
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.stem.porter import PorterStemmer
from nltk.tokenize import word_tokenize
from nltk.corpus import wordnet
from nltk import pos_tag

# package for loose matching words
from textdistance import levenshtein


def main(file):


	print('__launching__ \nstarting categories')

	toCategorise = loadFile(file)

	returnedCats = extractColumn(toCategorise, 'Category')
	# Category wizzardry needed to generate initial cat groupings below
	# Useless on mass upload instead could do the count method below
	topCats = categorySplitting(returnedCats)

	gc.collect()

	print('completed categories \nstarting lemmas / pos')

	# Keyword magic
	rawKeywords = extractColumn(toCategorise, 'Keywords')
	keywordBreakdown = keywordMagic(rawKeywords)

	print('completed lemmas / pos \nfinding top words')

	gc.collect()


	# Pull out list top words
	topWords = extractLemmas(keywordBreakdown, ignoreWords)

	print('combining')

	gc.collect()

	# Pull out factors into a list of list
	factoredTable = buildFactors(returnedCats, topCats, keywordBreakdown, POSGroups, topWords)

	wrieFile('nlpSearchLens.csv', factoredTable)




#-----------------------------------------------------------------------------------------

# Category / Groupings



POSGroups = {
			'other': 
				['CC', 'DT', 'EX', 'IN', 'TO', 'UH',],
			'numbers': 
				['CD', 'LS',],
			'describe': 
				['JJ', 'JJR', 'JJS', 'RP',],
			'noun': 
				['NN', 'NNS', 'NNP', 'NNPS', 'PDT', 'POS', 'PRP$',],
			'foreign': 
				['FW',],
			'actions': 
				['MD', 'RB', 'RBR', 'RBS', 'VB', 'VBD ', 'VBG', 'VBN', 'VBP', 'VBZ',],
			'question': 
				['WDT', 'WP', 'WP$', 'WRB', ],
			}

## This dunny do much good
# initialCatGroups = {
# 					'hobbiesCat':
# 						['Hobbies & Leisure', 'Cooking', 'Travel & Tourism', 'Arts & Entertainment', 'Wine & Beer Collecting & Brewing', 'Wine Collecting', 'Sports & Fitness', 'Sporting Goods', 'Bicycles & Accessories', 'Bike Parts & Repair', 'Sports', 'Bicycles', 'Road Bikes', 'Cycling','Toys & Games', 'Games', 'Online Games & Puzzles', 'Fun & Trivia', 'Children\'s Games & Activities',],
# 					'healthCat':
# 						['Health', 'Health Conditions & Concerns', 'Digestive Health & Disorders', 'Pharmacy', 'Vitamins & Supplements', 'Nutrition & Dieting', 'Weight Loss', 'Diets & Diet Programs', 'Weight Loss Diets & Diet Programs', 'Health-Related Restricted Diets', 'Non-Alcoholic Beverages', 'Alcohol Free Drink Mixers', 'Dairy & Non-Dairy Alternatives', 'Colitis', 'Dyspepsia', 'Heartburn', 'Indigestion & GERD Medications', 'Drugs & Medications', 'Sports & Fitness', 'Heart Health', 'High Cholesterol', 'Low Cholesterol Diets', 'Health Media & Publications', 'Health Care Services', 'Hospitals & Health Clinics', 'Hospitals', 'Non-Dairy Alternatives', 'Amino Acid Supplements', 'Baby Care & Hygiene', 'Pediatrics & Children\'s Health', 'Baby Health', 'Inflammatory Bowel Disease', 'Indigestion', 'Weight Loss Products', 'Weight-Loss Foods & Nutrition Bars', 'Meal Replacements', 'Alternative & Natural Medicine', 'Diabetic & Low Sugar Recipes', 'Blood Sugar & Diabetes', 'Special & Restricted Diet Foods', 'Cancer & Cancer Treatment', 'Cancer Medications', 'Chemotherapy Medications', 'Diabetes Management', 'Essential Fatty Acid & Omega-3s', 'Women\'s Health & OBGYN', 'Fetal Development', 'Medical Devices', 'First Aid Supplies', 'Injury & Wound Care', 'Antibiotic & Antiseptic Medications', 'Sexual & Reproductive Health', 'Family Planning', 'Fertility & Conception', 'Weight Loss Drugs & Medications', 'Cholesterol Medications', 'Nutrition Counseling', 'Insomnia & Sleep Disorders', 'Sleep Medications', 'Alcohol & Substance Abuse', 'Alcohol & Health', 'Biotech & Pharmaceutical',],
# 					'foodCat':
# 						['Cooking', 'Food & Groceries', 'Food', 'Recipes', 'Breakfast Foods', 'Breakfast Cereals', 'Beverages', 'Online Grocery Shopping & Grocery Delivery', 'Dairy & Non-Dairy Alternatives', 'Dining & Nightlife', 'Restaurants', 'Juice', 'Cereal & Grain', 'Non-Dairy Alternatives', 'Dine-In Restaurants', 'Non-Alcoholic Beverages', 'Alcohol Free Drink Mixers', 'Dining & Nightlife Reviews', 'Restaurant Reviews', 'Coffee', 'Bakeries', 'Cuisines', 'Mediterranean Cuisine', 'Food Processors', 'Yogurt', 'Desserts', 'Frozen Desserts', 'Frozen Novelties & Accompaniments', 'Soft Drinks', 'Weight-Loss Foods & Nutrition Bars', 'Meal Replacements', 'Diabetic & Low Sugar Recipes', 'Special & Restricted Diet Foods', 'Produce', 'Fruit', 'Dessert Recipes', 'Energy Drinks', 'Milk', 'Baby Food', 'Food Service Industry', 'Beverage', 'Candy', 'Gum & Mints', 'Vending', 'Vending Machines',],
# 					'shopsCat':
# 						['Retailers & General Merchandise', 'Online Grocery Shopping & Grocery Delivery', 'Retail Trade', ],
# 					'offersCat':
# 						['Coupons & Rebates','Product Reviews & Price Comparisons',],
# 					'articlesCat':
# 						['News', 'Media & Publications', 'Books & Literature', 'Health Media & Publications', 'Dining & Nightlife Reviews', 'Guides & Listings', 'Restaurant Reviews','Celebrities & Entertainment News', 'Consumer Resources', 'Community Service & Social Organizations', 'Product Reviews & Price Comparisons',],
# 					'alcoholCat':
# 						['Alcoholic Beverages', 'Wine', 'Liquor', 'Wine & Beer Collecting & Brewing', 'Wine Collecting', 'Beer', 'Mixers','Beverage & Bar Supplies', 'Bar & Wineware', 'Alcohol & Substance Abuse', 'Alcohol & Health',],
# 					'sportCat':
# 						['Sports & Fitness', 'Sporting Goods', 'Bicycles & Accessories', 'Bike Parts & Repair', 'Sports', 'Bicycles', 'Road Bikes', 'Cycling',],
# 					'homeCat':
# 						['Home & Garden', 'Kitchen & Dining', 'Home Appliances', 'Small Kitchen Appliances', 'Family & Community', 'Real Estate'],
# 					'travelCat':
# 						['Hotels', 'Motels & Resorts', 'Travel & Tourism', 'Accommodations', 'Bed & Breakfasts', 'Dining & Nightlife Reviews', 'Guides & Listings', 'Travel Booking Services', 'Vacation Packages', 'Vehicles', 'Travel Documents', 'Travel Consent Forms'],
# 					'industryCat':
# 						['Business & Industrial', 'Agriculture', 'Crops', 'Business Management', 'Advertising & Marketing', 'Equipment & Supplies', 'Finance', 'Insurance', 'Credit & Lending', 'Loans', 'Home Loans & Mortgages', 'Reverse Mortgages', 'Design & Engineering', 'Graphics & Multimedia Software', 'Visual Art & Design'],
# 					'petsCat':
# 						['Pets & Animals', 'Pets By Breed', 'Dogs', 'Pet Food & Supplies', 'Pet Medications',],
# 					'childrenCat':
# 						['Baby', 'Parenting & Family', 'Baby Feeding', 'Baby Formula','Maternity & New Parent', 'Baby Food', 'Fetal Development', 'Children\'s Games & Activities', 'Child Custody'],
# 					'educationCat':
# 						['Jobs & Education', 'Jobs & Careers', 'Job Listings','Community Service & Social Organizations','Education & Training', 'Teaching & Classroom Resources', 'Lesson Plans',],
# 					'electronicsCat':
# 						['Computers & Consumer Electronics', 'Consumer Electronics', 'Home Audio & Video', 'Televisions', 'Food Processors', 'Home Appliances', 'Small Kitchen Appliances','Computers', 'Software', 'Internet & Telecom', 'Internet', 'Internet Software & Web Goodies', 'Online Media', 'Telephony', 'Mobile Phones & Accessories', 'Mobile Phones', 'Smart Phones', 'Medical Devices', 'Photo Software', 'Graphics & Multimedia Software',],
# 					'selfcareCat':
# 						['Beauty & Personal Care', 'Apparel', 'Footwear', 'Boots', 'Hair Care', 'Skin Care'],
# 					'carCat':
# 						['Vehicles', 'Motor Vehicles', 'Cars & Trucks'],
# 					'govCat':
# 						['Community Service & Social Organizations', 'Law & Government', 'Legal', 'Family Law', 'Child Custody', 'Legal Forms & Kits', 'Travel Documents', 'Travel Consent Forms'],
# 					}



ignoreWords = ['for', 'and', 'a', 'the', '\'s', 'what', 'why', 'their', 'there', 'be', 'to', 'my', 'how', 'where', 'in', 'this']


#------------------------------------------------------------------------------------------

# Building out factors

def buildFactors(initialCats, topCats, keywordBreakdown, POSGroups, topWords):

	table = []

	table.append(['phrase', 'topCats', 'posFactors', 'topWordsFactors'])

	print('categories')

	categoryFactors = catListGeneration(initialCats, topCats)

	gc.collect()

	print('pos')

	posFactors = posGeneration(keywordBreakdown, POSGroups)

	gc.collect()

	print('top words')

	topWordFactors = topWordsLiklihood(keywordBreakdown, topWords)

	gc.collect()

	for index in range(len(categoryFactors)):

		val = list(keywordBreakdown[index].keys())

		phrase = ''.join(val)

		table.append([phrase, categoryFactors[index], posFactors[index], topWordFactors[index]])


	return table



def topWordsLiklihood(keywordBreakdown, topWords):

	topWordsList = []

	lemmaList = extractLemma(keywordBreakdown)

	for row in lemmaList:

		dicKeys = {}

		for word in topWords:

			sim = -1

			for value in row:

				simularity = levenshtein.normalized_similarity(value, word)

				if sim < simularity:

					sim = simularity

			dicKeys[word] = sim

		topWordsList.append(dicKeys)

		gc.collect()

	return topWordsList



def extractLemma(keywordBreakdown):

	listLemmas = []

	for row in keywordBreakdown:

		rowList = []

		for innerList in row.values():

			for value in innerList:

				rowList.append(value[0])

		listLemmas.append(rowList)

	return listLemmas




def posGeneration(keywordBreakdown, POSGroups):

	listPosCounts = []

	for row in keywordBreakdown:

		dicKeys = {}

		for key, listVals in POSGroups.items():

			keyCount = 0

			for innerList in row.values():

				for value in innerList:

					if value[1] in listVals:

						keyCount += 1

			dicKeys[key] = keyCount

		listPosCounts.append(dicKeys)


	return listPosCounts





def catListGeneration(initialCats, topCat):

	listKeyCounts = []

	for row in initialCats:

		dicKeys = {}

		for word in topCat:

			if word in row:

				value = 1

			else:

				value = 0


			dicKeys[word] = value

		listKeyCounts.append(dicKeys)

	return listKeyCounts




#------------------------------------------------------------------------------------------

# Top mentioned words


def extractLemmas(splitKeywords, ignoreWords):

	# Pulls out all the lemmas from the list dictionary created earlier
	allWords = extractWords(splitKeywords)

	# Creates a count of all the words appearing
	counts = Counter(allWords)

	# Removes all the words that are in the 'boring' / not informative work list (called ignoreWords above)
	extractedCounts = onlyInsightful(counts, ignoreWords)

	# Splits out top (1-x%) of counts
	topWords = cutOutPercent(extractedCounts, 90)

	return topWords


def extractWords(splitKeywords):
	all_words = []

	for row in splitKeywords:
		for values in row.values():
			for valuepair in values:
				all_words.append(valuepair[0])

	return all_words


def onlyInsightful(countedList, ignoreWords):
	updatedList = {}

	for key, value in countedList.items():
		if key not in ignoreWords:
			updatedList[key] = value

	return updatedList


def cutOutPercent(counts, perc):
	numbers = []

	for value in counts.values():
		numbers.append(value)

	numbers = np.array(numbers)

	cutOff = np.percentile(numbers, perc)


	# All words that is greater or equal to cut off

	words = []

	for key, value in counts.items():

		if value >= cutOff:
			words.append(key)

	return words



#--------------------------------------------------------------------------------------------

# Less fun keyword times

def keywordMagic(rawKeywords):
	breakdownPhrases = phrasesToWords(rawKeywords)
	return findWordInfo(breakdownPhrases)
	


# Strips phrases into the words

def phrasesToWords(listPhrase):
	length = len(listPhrase)
	i = 1
	phrase_list_dict_words = []
	for phrase in listPhrase:
		print(f'stripping phrase {i} of {length}')
		phrase_list_dict_words.append({phrase: word_tokenize(phrase)})
		i += 1
	return phrase_list_dict_words


# Pulls out the info for each word

def findWordInfo(dictWords):
	length = len(dictWords)
	i = 1
	dictPhraseLem = []
	for row in dictWords:
		print(f'finding info on words in phrase {i} of {length}')
		for phrase, listWords in row.items():
			posOfWords = findPOS(listWords)
			lemmaList = lemmatization(posOfWords)
			# definition = getDefinition(lemmaList)
			dictPhraseLem.append({phrase: lemmaList})
			i += 1

	return dictPhraseLem


# Finds the word position of speach

def findPOS(listWords):
	wordsWithPOS = []
	for row in pos_tag(listWords):
		identifierPOS = convert_pos(row[1][0])
		wordsWithPOS.append((row[0], identifierPOS, row[1]))
	return wordsWithPOS


# Create POS Tag

def convert_pos(posTag):
	tag_dict = {"J": wordnet.ADJ,
				"N": wordnet.NOUN,
				"V": wordnet.VERB,
				"R": wordnet.ADV}
	return tag_dict.get(posTag, wordnet.NOUN)

# Reverts word to the stem

def stemmatization(listWords):
	listDictStem = []
	stem = PorterStemmer()
	for row in listWords:
		listDictStem.append(stem.stem(row[0]))
	return listDictStem


# Reverts words to their root (supposedly ... but not well)

def lemmatization(listWords):
	listdDictLem = []
	lem = WordNetLemmatizer()
	for row in listWords:
		listdDictLem.append((lem.lemmatize(row[0], row[1]), row[2]))
	return listdDictLem


# Looks at word definitions -- currently not useful because it's over complicated

# def getDefinition(listWords):
# 	summaryDefinitions = []
# 	for word in listWords:
# 		print(word[0])
# 		try:
# 			print(wordnet.synsets(word[0])[0].definition())
# 		except:
# 			print('None')

#--------------------------------------------------------------------------------------------

# Fun category times

def categorySplitting(listCategories):
	# uniqueCategoryIdentifiers = []
	allCategoryIdentifiers = []
	for row in listCategories: 
		for value in row.split(', '):
			allCategoryIdentifiers.append(value)
			# if value not in uniqueCategoryIdentifiers:
			# 	uniqueCategoryIdentifiers.append(value)

	counts = Counter(allCategoryIdentifiers)

	topCats = cutOutPercent(counts, 95)

	return topCats


#--------------------------------------------------------------------------------------------

# Splitting data

def extractColumn(dic, key):
	extractedList = []
	for row in dic:
		extractedList.append(row[key])
	return extractedList
		

#-------------------------------------------------------------------------------------------

# File loading to be seperated below

def loadFile(file):
	rows = []

	with open(file, "r", newline='') as csv_file:
		csv_reader = csv.reader(csv_file, delimiter=',')
		for row in csv_reader:
			rows.append(row)


	rows = writeToListDict(rows)

	return rows


def writeToListDict(listList):
	listDict = []
	headers =  listList[0]

	ll = len(listList)
	lh = len(headers)

	il = 1
	while il < ll:
		ih = 0
		newDict = {}
		while ih < lh:
			try:
				newDict[headers[ih]] = listList[il][ih]
				ih += 1
			except:
				newDict[headers[ih]] = None
				ih += 1
		listDict.append(newDict)
		il += 1
	return listDict


def wrieFile(filename, listList):

	with open(filename, "w", newline='') as f:
	    writer = csv.writer(f)
	    writer.writerows(listList)


#-------------------------------------------------------------------------------------------

# Main function

if __name__ == '__main__':
	main('testingUpload.csv')